Dino's Ultima Page - Ultima Underworld 2: Labyrinth of Worlds












&#9776; Navigation


Main

Latest News
Old News
Credits


Feedback

Contact
Discussion


Links

WebDB2
Hosted Sites


Information

General
Ultima I
Ultima II
Ultima III
Ultima IV
Ultima V
Ultima VI
Savage Empire
Martian Dreams
Underworld I
Ultima VII
Underworld II
Serpent Isle
Ultima VIII
Ultima IX
Mount Drash


0xdeadbeef

Ultima X
Lord of Ultima




Ultima Underworld II: Labyrinth of Worlds
Bugs and Mistakes
Ultima Underworld II inaccuracies [English | German] are available at Hacki's Ultima Page [English | German].
There seems to be a bug with the automap system where any notes you write beyond a specified limit are lost once you close the map.
Concentrations
Website sections dedicated to Ultima Underworld II can be found:
Here at Bootstrike
Here at the Ultima Web Archive
Here at Sir Cabirus' Ultima Homepage

Downloads
Peroxide, that was making Ultima 1 - A Legend is Reborn, has developed an Ultima Underworld Viewer for both Ultima Underworld I and II. It can be downloaded from their page, directly from here [ZIP].
Legacy
Pix's Origin Adventures released an Ultima Underworld 3 Design Document on 1st June 2018. The introductory bullet points can be found here at GameBanshee. Like several other Origin intellectual properties, Ultima Underworld 3 never saw the light of day, and this document provides insight into what could have been.
Magic
Sir Cabirus' Ultima Underworld II walkthrough has a list of undocumented spells, and a separate list of runestones.
Maps &amp; Places
The map of Lord British's castle can be found:
Here at the Ultima Web Archive
Here at Auric's Ultima Moongates
Maps of all the levels can be found at:
Arthuris Dragon's Ultima Maps and Tables
Elenwhir Dragon's Maps for "Labyrinth of Worlds" (thanks to the Internet Archive)
Crowley's "Guardian's antics as seen in UW2" gives a good description of the places (as well as the story) in Ultima Underworld 2.
Pictures
Pictures of the box cover can be found at the Underworlds section of the Ultima Web Archive.
Services
Sir Cabirus' Ultima Underworld II walkthrough has a list of Trainers, including the skill they train you in, and their location.
Summaries and Reviews
Crowley's "Guardian's antics as seen in UW2" gives a good description of the story and places in Ultima Underworld 2.
Natreg Dragon pointed out a nice video review of the game by Excalibur Reviews.
Rampant Coyote wrote a "Reminiscing about Ultima Underworld" piece on 3rd June 2011.
Walkthroughs
Mitch Aigner's Ultima Underworld II walkthrough has lots of useful information and can be found:
Here at the Ultima Web Archive
Here [TXT] at Underworld Adventures
Sir Cabirus launched a new Ultima Underworld II walkthrough towards the end of 2015, complete with interactive maps and screenshots.
Kikoskia's Let's Play Ultima Underworld 2: Labyrinth of Worlds is a video playthrough of the game (thanks Vividos for finding this).


&copy; by Daniel D'Agostino 2002-2021