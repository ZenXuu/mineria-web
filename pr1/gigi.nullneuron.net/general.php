Dino's Ultima Page - General Ultima Information












&#9776; Navigation


Main

Latest News
Old News
Credits


Feedback

Contact
Discussion


Links

WebDB2
Hosted Sites


Information

General
Ultima I
Ultima II
Ultima III
Ultima IV
Ultima V
Ultima VI
Savage Empire
Martian Dreams
Underworld I
Ultima VII
Underworld II
Serpent Isle
Ultima VIII
Ultima IX
Mount Drash


0xdeadbeef

Ultima X
Lord of Ultima




General Ultima Information
Contents

General Ultima Information
Concentrations
Documentation
Ethics
Fanfiction
Maps and Places
Media
Music
People
Trivia

About Ultima / Opinions
Learn about Ultima, read what people said about it, and post your opinion at the
About Ultima / Opinions page.
Concentrations
Encyclopaedias
The links below used to hold large quantities of Ultima-related information. Alas, they are no
longer available, but they remain here for a bit longer so that I can keep track of them and
perhaps find a way to restore them.

Ultima - The Book of Lore (at UXO Stratics forums)
Encyclopaedia Britannia (at The Wayward Avatar)
Avocet Dragon's Ultima Encyclopaedia (at Avocet Dragon's Journey to Sosaria)

Documentation
Copy Protections
Check out my list of copy protection questions and answers from all the Ultimas.
Manuals

The Ultima Web Archive provides the Ultima 1-6 Encore Documentation that was left out of the CD.
Micro Dragon has rewritten the Ultima 1-6 Documentation in Word format.
Paulon Dragon's Ultima Info used to host Micro Dragon's Ultima 1-6 Documentation (Internet Archive) after Micro's site had vanished some time ago.
Bootstrike hosts manuals for each Ultima.
You can view The Ultima [6] Compendium online in a very attractive and interactive format.

Ethics
Ultima 4 revolutionised the RPG genre with the introduction of a system of
virtues that would be affected positively or negatively depending on what actions
you choose to do.
The eight virtues stemmed from the combinations of three principles, which
in turn were based on the underlying principle of infinity.
Although these virtues remained present in Ultima until the end, there were
also other virtue systems introduced throughout the course of the series. In
Ultima 6, the gargoyles turned out to have a similar system of beliefs. Also in
Ultima 6, a man named Mandrake devised his own virtues from the principles of
Wine, Women and Song. Serpent Isle introduced a new virtue system with a
totally different combination system, leading to nine virtues.
Related Links

Britannian virtues: "The Virtues" at Silva's Ultima Site
Britannian Virtues: "Virtues" at the EUO Manual
All virtues: "Virtues of Ultima" at Wikipedia Encyclopaedia
Serpent Isle virtues: "Beliefs of Serpent Isle"
"The Story Behind Ultima’s Morality" at Kotaku (4th July 2018)

Fanfiction
Three fanfictions are hosted on Dino's Ultima Page:

The Lost Vale
Tale of the Mystics
The Return

For other Ultima fanfiction, please visit the following sites:

Dragon Press (maintained by Shadow of Light Dragon)
Lair of the Evil_Freak (maintained by Evil_Freak Dragon)

Maps and Places
Place Descriptions

Telavar Dragon's Mysterious Sosaria allows you to explore the world of Britannia through an interactive map. Each place is beautifully rendered and described.
Underworld Dragon's Notable Ultima has an essay about Stonegate.

Maps

Xe Dragon's High Resolution Ultima Map Project (HRUMP) provides beautiful scans of the cloth maps from each game.
Micro Dragon's Home Page also provides scanned cloth maps.
Arthuris Dragon's Ultima Maps and Tables has excellent maps of most of the Ultima games, many of which are unique and among the best on the net.
The Ultima Web Archive provides maps of each game; some are scanned cloth maps, and others are computer-generated.
Fallen Angel Dragon's "The Lands of Ultima" has complete and interactive (though very large) maps of Ultimas 1, 3 and 5.
The Ultimatrix Homepage has a maps section displaying the map-generating power of Ultimatrix.

Map-Viewing Programs

Xenerkes Dragon's Ultimatrix allows you to view, explore and generate maps of Ultimas 1-6, Savage Empire and Martian Dreams.
Exult does not only allow you to teleport across the world in Ultima 7 (Black Gate and Serpent Isle both included) by pressing F3 and clicking on a location on the world map, but it can also be used to generate huge world maps. See the "Command Line Options" section in the Exult Documentation to learn how to generate Ultima 7 world maps using Exult.
The Pentagram Map and Shapes Viewer allows you to explore the Ultima 8 maps. See my Ultima 8 Utilities page to learn more about it, as the map and shapes viewer is now obsolete and is being replaced by the Pentagram Ultima 8 Engine.

Media
Ringtones
Some Ultima music can be used as a ringtone for your mobile phone. If your
mobile phone is WAP-
or GPRS-enabled,
use it to go to
http://gigi.nullneuron.net/ultima/ugeneral/tones/.
The ringing tones are provided for free. The only costs you might have to
incur are those of using WAP or GPRS to download them.
The ringing tones currently provided are:

"Gwani" (from SI)
"Emp" (from U7)
"Musicbox" (from U7)
"Rule Britannia" (from U7)
"Sea Shanty" (from U7)

Wallpapers



The above wallpaper was sent to me by W G, with the following email:

I see you are an Ultima fan, by your web page. I made a fanart desktop with all reconizable faces from Ultima games. I wanted to share it with the ultima community.

Christmas Cards


When there's no chimney around, Santa can still get in somehow.


Baby Santa, complete with dumb expression and toy.

Music
The following are locations of downloadable Ultima music:
The Bard's Library at Mysterious Sosaria - the greatest resource of Ultima music
The Ultima Web Archive - a taste of Ultima music from the whole series
C64 music remakes from Ultima 3-6 (submitted by Hacki Dragon)
Dor-L&#243;min - Ultima 6 MP3s
Ultima remixes at Bootstrike's Ultima section
Xelar Dragon's Ultima mixes page - Ultima 6 and 7 music remakes
Ymochel the Drow's website - more Ultima music
The joxter.de Stones archive - specialises on Stones, containing the many forms of Stones (whether official or fan made for remakes) as well as the lyrics
Steven Keys' website - Ultima music in .ogg format
Ultima Lives - MIDI music from the whole Ultima series
Tom's Ultima MP3 Collection - lots of MP3s from the Ultima series
Remix.Kwed.Org has a few remixes from Ultima 3 and 4 by Petrushka

For the more musically inclined, check out the Ultima Songbook [PDF, ~7.92MB] by Stratocaster Dragon (hosted here with his permission).
Sheet music for Ultima 4 has been transcribed by Yofa Dragon. A video of the music (with sheet music download links) is available on YouTube. Copies of the sheet music are available here at Dino's Ultima Page with permission (NTSC tuning [PDF] and PAL tuning [PDF]).
People
Ultima Developers
General info:

Underworld Dragon's Notable Ultima: Origin People
Underworld Dragon's Notable Ultima: Origin History
The Digital Antiquarian: "Origin Sells Out" (6th September 2019) is about EA's acquisition of Origin and what happened shortly thereafter

Richard Garriott:

Websites

Richard Garriott


Biographies

Biography at Wikipedia
Biography at Richard Garriott's website


Interviews

GameSpy - "From Origin to Destination" (7th May 2004)
GamaSutra - "Richard Garriott is from Mars" (8th February 2008)
"The Space-Family Garriott: An Interview with Lord and Lady British" (4th September 2015)
"Interview with Richard "Lord British" Garriott on serendipitous story-telling" by Joseph Drasin aka Unseen Dragon (7th September 2016)
"Ultima Creator on What It's Actually Like to Go to the Bathroom in Space (IGN Unfiltered)" (14th March 2017)
"EA Shot Down Ultima Online Pitch 3 Times (IGN Unfiltered)" (21st March 2017)
"That Time Richard Garriott Pointed an Uzi at a Guy (IGN Unfiltered #17, Episode 2)" (22nd March 2017)
"Spam Spam Spam Humbug 94 – The Lost Richard Garriott Interview" (audio, recorded in 2014, published on 14th December 2017)
"Episode 56: Richard Garriott (Ultima, Shroud of the Avatar) Interview" at The Classic Gamers Guild Podcast (10th December 2019)
"Richard Garriott's Ultima Story - Retro Tea Break" (13th April 2020) covers the history of the Ultima series; the origins of Lord British, Origin and Ultima Online; and a bit of Shroud of the Avatar


Pictures

The Almighty Guru's Ultima section: lots of pictures of Richard Garriott
Richard Garriott's website: photos



Dr. Cat:

Interviews

Matt Chat 361: Dr. Cat Part I (4th December 2016)
Matt Chat 362: Dr. Cat on Origin Systems (11th December 2016)
Matt Chat 363: Dr. Cat Part III (15th January 2017)
Matt Chat 364: Dr. Cat on Ultima 6 and 7 and Life at Origin Systems (23rd January 2017)
Matt Chat 367: Dr. Cat on DragonSpires and the first MMOs (12th February 2017)
Matt Chat 368: Dr. Cat on Furcadia (20th February 2017)


Conference Talks

"Math for Game Programmers: Random Tidbits: Use Chance to Enhance Fun!"



Sheri Graner Ray:

Blogs

FEM IRL (new blog)
FEM IRL (old blog)



Ultima Dragons Internet Chapter

The Ultima Dragons Internet Chapter (UDIC) is
the largest Ultima fanclub on the net. While the actual site, maintained by
Fallible Dragon,
is far from active, the rosters
(chronological
or alphabetical) are still
active, boasting over 15,000 members.
Becoming a Dragon doesn't give you any special power or rights, but lets you bear
the "Dragon" suffix to your nickname, and makes you a member of the evergrowing
fanclub.
Parcival at UltimaDot had compiled a series of biographies about Ultima Dragons who have given an outstanding contribution to the Ultima series, called the Dragon Gallery. Although UltimaDot is long gone, you can still read them thanks to a mirror spotted by Houston Dragon:
Daniel D'Agostino aka Dino the Dark Dragon (8th November 2003)
Christian Hackl aka Hacki Dragon (17th November 2003)
Laura Campbell aka Shadow of Light Dragon (26th November 2003)
John Hosie aka Houston Dragon (6th December 2003)
Paul Ryan aka Paulon Dragon (24th December 2003)
Paul Gilham aka Samurai Dragon (17th January 2004)
Ron Windeyer aka Gaseous Dragon (26th February 2004)
Joseph Morris aka DOUG the Eagle Dragon (31st July 2004)

Game Characters

Lord British
The Guardian
Shamino

Read about his origins and check out his D&amp;D character sheet at The Ultima Codex.



Trivia
Quizes and Tests

Quill Dragon's Lair has a few virtue tests as well as a vice test. Visit the Path of the Powerplayer for quick access.
The Lair of the Evil_Freak has an Ultima trivia quiz.
Dino's Ultima Page has its own general Ultima quiz.
Underworld Dragon's Notable Ultima has a section called The Trivial Ultima, which is in itself a quiz.
The Trinsic Telegraph has an Ultima crossword puzzle.
An Ultima Personality Test can help you see towards which virtues you are inclined most.

Stories

Dupre's duck:

Ducks in Ultima at Dino's Ultima Page
"Richard Garriott Tells the "Wanna Buy a Duck" Story" (YouTube video) at the UDIC 25th Anniversary Bash


The Bar that Killed the King (aka The Plaque of Doom) at The Origin Museum
Rooms full of killing children, part of an interview with Richard Garriott

By Game

Ultima 3 Trivia at the Codex of Editable Wisdom
Ultima 4 Trivia at Mobygames
Ultima 6 Trivia at Mobygames
Savage Empire Fun at Dino's Ultima Page
Savage Empire Cameos at Wikipedia
Martian Dreams Trivia at Wikipedia


&copy; by Daniel D'Agostino 2002-2021