Dino's Ultima Page - Lord of Ultima












&#9776; Navigation


Main

Latest News
Old News
Credits


Feedback

Contact
Discussion


Links

WebDB2
Hosted Sites


Information

General
Ultima I
Ultima II
Ultima III
Ultima IV
Ultima V
Ultima VI
Savage Empire
Martian Dreams
Underworld I
Ultima VII
Underworld II
Serpent Isle
Ultima VIII
Ultima IX
Mount Drash


0xdeadbeef

Ultima X
Lord of Ultima




Lord of Ultima
Contents

Background
Facts
In the News
Reception
Screenshots

Background
From the official website:
Lord of Ultima is an online strategy game where you build your own unique city and play with thousands of other players. Join now and rule a mighty empire which you can access from wherever and whenever you want.
All you need to enter is a normal web browser...

Facts

Official website: https://www.lordofultima.com/
First appeared on: 26th January 2010
Status: live (released 19th April 2010)
Genre: online strategy
Platform: web browsers
Developer: EA Phenomic

In the News

Gamasutra: EA Phenomic Revives Ultima With Browser-Based Strategy Game (26th January 2010)
Rock, Paper, Shotgun: You're Not Going To Like This: Ultima's Back (26th January 2010)
Worlds in Motion: EA Phenomic Revives Ultima With Browser-based Strategy Game (26th January 2010)
Blue's News: Lord of Ultima in Beta (26th January 2010)
Big Download: Ultima franchise gets new Lords of Ultima browser-based strategy game (26th January 2010)
Voodoo Extreme: Lord Of Ultima Open Beta Begins (26th January 2010)
Aiera: Lord of Ultima (26th January 2010)
La Legende d' Ultima: News - Lord of Ultima Annoncé (27th January 2010)
Play BBG: Lord Of Ultima: EA Regenerates Browser Game in Medieval Ages (27th January 2010)
Softpedia: Electronic Arts Revives Ultima Series with Online Strategy Title (27th January 2010)
Kotaku: Ultima Returns! Now For The Bad News... (27th January 2010)
MCV: EA resurrects Ultima franchise (27th January 2010)
Dino's Ultima Page: Lord of Ultima; U6P Beta 2 Release (30th January 2010)
Dino's Ultima Page: Lord of Ultima and Pagan The Beautiful (3rd March 2010)
PC Gamer: "Lord of nothing to do with Ultima" (intro/full review) (25th June 2010)

Reception
Reception from long-standing Ultima fans seems to be quite poor, as can be
seen from the articles above, and discussions below:

RPGWatch Forums - EA - Lord of Ultima
Aiera - Lord of Ultima (comments)
Aiera's Sword an Shield - Lord of Ultima
Blue's News - Lord of Ultima in Beta (comments)
NAG Online Forums - Lord of Ultima
UO Stratics Forums - Ultima returns...

Screenshots
Note that screenshots are hard to come by because people participating in
the closed beta are not allowed to distribute any.

Aiera: Lord of Ultima (26th January 2010)


&copy; by Daniel D'Agostino 2002-2021