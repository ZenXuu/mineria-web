Dino's Ultima Page - Ultima VII Part 2 (Serpent Isle)












&#9776; Navigation


Main

Latest News
Old News
Credits


Feedback

Contact
Discussion


Links

WebDB2
Hosted Sites


Information

General
Ultima I
Ultima II
Ultima III
Ultima IV
Ultima V
Ultima VI
Savage Empire
Martian Dreams
Underworld I
Ultima VII
Underworld II
Serpent Isle
Ultima VIII
Ultima IX
Mount Drash


0xdeadbeef

Ultima X
Lord of Ultima




Ultima VII Part 2 - Serpent Isle
Bugs and Mistakes
Serpent Isle inaccuracies [German|English] - Serpent Isle Nitpicks at Hacki's Ultima Page [German|English].
Culture






&quot;The Ophidian Alphabet was used by the previous inhabitants of the Serpent Isle, before the balance was upset. You'll encounter it frequently while exploring the ruins.&quot;Source: The Ultima Web Archive


For more information about Ophidian Culture, check out The
Principles and Virtues of Balance, Chaos and Order at Houston
Dragon's Ultima Page.
Documentation
The Serpent Isle copy protection questions and answers are available on my copy protections document.
Characters and Dialogues
Houston Dragon's Ultima Page has A Who's Who of Serpent Isle. It shows in detail the many characters in Serpent Isle, as well as their portraits and their role.
Concentrations
Serpent Isle sections on websites can be found:
Here at the Ultima Web Archive
Here at Bootstrike
At Arthuris Dragon's Ultima Maps and Tables with some very good maps
Here at the Other Codex (Internet Archive)
Here at Auric's Ultima Moongates
At the Serpent Isle Wiki at StrategyWiki

Fun
Resulka has a video showing the garbage that NPCs say when you fail the copy protection test.
Items
A list of weapons and armour statistics are on the Other Codex (Internet Archive).
You can find a list of lost items (taken away by the teleport storm) at Mike's RPG Center. There is also a list of merchandise with prices.
Magic
Mike's RPG Center has a list of spells including the reagents they require, their prices and locations. There is also a separate page about reagents.
Maps
Serpent Isle (World) Paper/Cloth Maps






&quot;Note the similarities to Ultima I's Lands of Danger and Dispair [sic]. This is not a coincidence: the Serpent Isle is one of the three lost continents of Britannia.&quot;Source: The Ultima Web Archive






&quot;This is a large scanned version of the PAPER map that came with the Electronic Arts release of SI, not of the cloth map that was included in the OSI release.&quot;Source: UltimaInfo.Net






Serpent Isle mapSource: Blackshadow Dragon's Ultima Warehouse


Other paper/cloth maps of Serpent Isle can be found as follows:
World map at Auric's Ultima Moongates
At the High Resolution Ultima Map Project in 4 different sizes (these are the nicest Serpent Isle maps)

Serpent Isle (World) Computer-Generated Maps
Maps of the actual world as it is in the game are:
At Arthuris Dragon's Ultima Maps and Tables (HUGE)
At Kenn's Map Page in 2 sizes

Maps of Gorlab Swamp
A map of Gorlab Swamp can be found:
Here at Arthuris Dragon's Ultima Maps and Tables
Here at Bootstrike

Maps of the Dark Path
Maps of the Dark Path, which is the central point of the Serpent Gates, can be found:
Here [TXT] at Arthuris Dragon's Ultima Maps and Tables
Here (relatively large .jpg image turned 90 degrees anticlockwise) at the home of the Serpent Isle original plot docs
At some other walkthroughs; see the walkthroughs section below for links to walkthroughs
Here (as seen in game) at Dino's Ultima Page

In-Game Maps
I have a set of in-game maps here at Dino's Ultima Page, from when I played the game
several years ago. I don't really remember which map is which.











The Dark Path






Ice Plains











Shamino's Castle


You can also find these maps along with more information about them at the
Maps in
Serpent Isle page at The
Codex of Ultima Wisdom.
Music
You can download Serpent Isle music:
From the Serpent Isle music page at Mysterious Sosaria
In MP3 form at the MP3s page of The World of Ultima
Here [ZIP] at the Ultima Web Archive

A patch [ZIP] for general midi compatibility is on the Ultima Web Archive.
Omissions
The original plot of Serpent Isle, complete with cut-out parts, related to Moonshade and Mad Mage Isle has been found.
Also, the following topics at the Exult forum contain details about omitted parts of the plot:
"Hidden conversation in SI"
"Again Serpent Isle plot."
"Serpent Isle cut plot... (notes)"
Sheri Graner Ray wrote the following about the original plot of Serpent Isle in her blog post "Looking back on 20 years in the industry":
** Originally Bill Armintrout and I were doing the initial design on Serpent Isle. The mandate we were given by Richard and Jeff George (the producer at that time) was that it was to be about the conflict between Brittanian magic and VoDun (VooDoo) magic. And that the island was to be called Serpent Isle because it we were suppoesed to make it in the shape of the snake necklace that Richard wore (and still does, I think.) So I spent a month at the UT LIbrary checking out and reading books on VoDun as that was my side of the design. We'd been in design about three or four months when there was a "re-org." Jeff George quit and the game was given to Warren Specter to produce. We were told to essentially toss everything out and start over. I recently gave the maps and docs from the earliest part of that design to the Origin Museum.
** It was during the reset of Serpent Isle that I came up with the Order + Chaos = Balance religion that made up the base of the current Serpent Isle. I had it all on a white board in the office and was trying to explain it to Bill Armintrout. Bill refused to see it and kept saying "You can't add things that aren't numbers. This makes no sense. Things like that don't add together. " I got SOO frustrated with trying to explain it that I pulled in Brendan Seagraves and explained it to him. He saw that it made perfect sense and then he explained it to BIll.. and only then did Bill "get it" and agree to put it in the game. Prem Kirshnan and I then worked on putting it together and coming up with the final components.
The Cutting Room Floor has a whole list of unused data and artwork in Serpent Isle, ranging from character portraits to unused items.
People
Who's Who in Serpent Isle by Houston Dragon contains detailed descriptions of every character in Serpent Isle, including their portrait and role.
There's a list of people with names, jobs, locations and notes at Mike's RPG Center.
Several NPCs' portraits were modelled around real people as a result of the Immortality Contest. Chastain Dragon is one of these, and on his Serpent Isle page, he writes about how he became Flindo in Serpent Isle.
Pictures
There are some Serpent Isle Pictures on the World of Ultima.
This is where to find box cover art:
Serpent Isle cover art and Silver Seed cover art at Auric's Ultima Moongates
A small box cover at the Guardian saga section of the Ultima Web Archive

Remakes and Engines

Exult is the engine to use to run Ultima VII (Serpent Isle included) under virtually any modern platform. Read my review for more information.
Ultima VII: Chronicles of Batlin was supposed to be a new game using the Exult engine where you play as Batlin, and it would have taken place before the Avatar came upon Serpent Isle.
Ultima 7: Blackrock was a project using the Exult engine which aimed to remake The Black Gate and Serpent Isle, adding the long-forgotten Serpent Isle original plot and elaborating on elements in The Black Gate that could have been expanded further.
Ultima Online: Serpent Isle is an effort to port Serpent Isle to Ultima Online. Read more about it at its WebDB2 entry or at the project's homepage.

Walkthroughs
Adrian Yau has written a walkthrough for Serpent Isle. It can be found:
Here at the Ultima Web Archive
Here at Bootstrike

Fallible Dragon has also written a walkthrough for Serpent Isle, which can be found:
Here at the Ultima Web Archive
Here at Bootstrike

Gary Thompson's walkthrough for the Silver Seed can be found at Bootstrike.
The Serpent Isle walkthroughs page at Bootstrike contains many other walkthroughs, including among others:
Serpent Isle walkthrough by Lord DCD
Serpent Isle walkthrough by Anthony Salter of Origin
Silver Seed walkthrough by Michael Sonntag

There's another Serpent Isle walkthrough in point form at Mike's RPG Center.


&copy; by Daniel D'Agostino 2002-2021