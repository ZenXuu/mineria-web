Dino's Ultima Page - Ultima 2 (Revenge of the Enchantress)












&#9776; Navigation


Main

Latest News
Old News
Credits


Feedback

Contact
Discussion


Links

WebDB2
Hosted Sites


Information

General
Ultima I
Ultima II
Ultima III
Ultima IV
Ultima V
Ultima VI
Savage Empire
Martian Dreams
Underworld I
Ultima VII
Underworld II
Serpent Isle
Ultima VIII
Ultima IX
Mount Drash


0xdeadbeef

Ultima X
Lord of Ultima




Ultima II - Revenge of the Enchantress
Concentrations
Ultima II sections on websites can be found:
Here at the Ultima Web Archive
Here at Auric's Ultima Moongates
Here at the Codex of Ultima Wisdom.

Bestiary
Enemies from both the overworld and in dungeons are listed at at the RPG Classics Ultima 2 Shrine.
The main foe is Minax (Codex of Ultima Wisdom page).
Documentation
The Ultima II Encore CD-ROM Game Manual is on the Ultima Web Archive.
Another Ultima II manual [TXT] used to be at Quill Dragon's Lair, but is now being hosted here since that site is no longer available.
File Format Information
The C64 tileset of Ultima 2 was extracted by
Minstrel Dragon, who kindly agreed to have it hosted here at Dino's Ultima Page.
Items
The following special items appear in Ultima 2:
Quicksword (at the Codex of Ultima Wisdom)
Force Field Ring (at the Codex of Ultima Wisdom)

Items left by slain enemies are listed at the RPG Classics Ultima 2 Shrine.
Maps
Maps of Earth can be found:
Here at Auric's Ultima Moongates
At the Ultima II maps page of the Ultima Web Archive
Space maps can be found:
At the Ultima II maps (with notes underneath) at the Ultima Web Archive
There are Ultima II maps of all the planets and times at an Internet Archive snapshot of Virtues Dragon's Codex of Ultimate Wisdom.
Plots and Summaries
Find out more about the story of Ultima II:
Here at Auric's Ultima Moongates
At the Ultima 2 Page of the Codex of Ultima Wisdom
At "Reconciling the Past: Revenge of the Enchantress" at The Digital Lycaeum
At the MS-DOS Project's video review (YouTube, 11th May 2020) of the game
In the book Through the Moongate by Andrea Contato
At The Digital Antiquarian's two-part exploration of Playing Ultima II (Part 1, Part 2)
In Felipe Pepe's review in The CRPG Book

Walkthroughs
Fallible Dragon has written a walkthrough for Ultima 2. It can be found at the Ultima Web Archive.
Clues for finishing the game by Adric can be found in:
HTML at the Ultima Web Archive
text at Auric's Ultima Moongates


&copy; by Daniel D'Agostino 2002-2021