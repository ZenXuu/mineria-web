Dino's Ultima Page - Ultima Worlds of Adventure 2 (Martian Dreams)












&#9776; Navigation


Main

Latest News
Old News
Credits


Feedback

Contact
Discussion


Links

WebDB2
Hosted Sites


Information

General
Ultima I
Ultima II
Ultima III
Ultima IV
Ultima V
Ultima VI
Savage Empire
Martian Dreams
Underworld I
Ultima VII
Underworld II
Serpent Isle
Ultima VIII
Ultima IX
Mount Drash


0xdeadbeef

Ultima X
Lord of Ultima




Ultima Worlds of Adventure 2 - Martian Dreams
Bugs and Mistakes
Hacki's Ultima Page [German | English] has some Martian Dreams nitpicks [German|English].
Concentrations
Martian Dreams sections can be found:
Here at the Ultima Web Archive
Here at The Other Codex (Wayback Machine)

Martian Dreams pages can be found:

Here at Wikipedia
Here at the Codex of Ultima Wisdom

Documentation
The Martian Dreams copy protection questions and answers are available on my copy protections document.
Fun
The Martian Dreams page at Wikipedia has a list of cameos and trivia.
More trivia can be found at the Martian Dreams page at the Codex of Ultima Wisdom.
The word "GRYPHON", written into the ice caps of the Martian Dreams map, is a tribute to the main map designer on the Martian Dreams team.
Items
Thanian Dragon and Paulon Dragon have compiled a list of items [TXT] obtainable with MDHack. This list is available at The Other Codex (Wayback Machine).
Maps and Places
A tiny, illegible cloth map of Mars can be found:
Here at the Ultima Web Archive
Here at Auric's Ultima Moongates
Nice big cloth maps of Mars can be found at the High Resolution Ultima Map Project.
There is a whole page about places and interesting encounters at UltimaInfo.net (Internet Archive).
Computer-generated maps can be found at Andrew Jenner's Ultima maps page.
The face on Mars is a hill on the surface of Mars shaped like a face, which both features in the game and really exists. The following is a list of related resources.
In-game screenshot here at Dino's Ultima Page
Location on Mars on Google Mars
Background (and pictures) about The "Face on Mars" at Malin Space Science Systems

MDHack
See my page about MDHack at my Savage Empire Guide.
Thanian Dragon and Paulon Dragon have compiled a list of items [TXT] obtainable with MDHack. This list is available at The Other Codex (Wayback Machine).
There is an MDHack entry at The Ultima Codex.
Media
Screenshots of Martian Dreams can be found:

At the Martian Dreams page at the Codex of Ultima Wisdom.
At the Martian Dreams page at Wikipedia.
At the Let's Play Martian Dreams walkthrough, in large quantities.

Indie Gamer has a post about Martian Dreams (9th January 2020). It contains a brief description of the game as well as a YouTube video in which she plays the introduction and first moments of the game.
Music
A whole selection of Martian Dreams music is available at The Bard's Library, part of Mysterious Sosaria.
People
From the Martian Dreams section at The Other Codex (Wayback Machine), you can download a zip file containing icons made from the portraits of the Martian Dreams NPCs.
Projects
The Ultima Codex has a list of projects related to Martian Dreams.
Reviews
Read about Martian Dreams at:
"CRPG History Abridged - 21 RPGs that brought something new to the table", by Felipe Pepe for Gamasutra (25th June 2015).
The CRPG Book project's Martian Dreams review, a preview of which can be found here at The Ultima Codex.

Story
The story of Martian Dreams can be read:
At the Martian Dreams page at the Codex of Ultima Wisdom
At the Martian Dreams page (by Robert "Bladed Edge" Kosarko on 23rd October 2012) at Hardcore Gaming 101's Ultima series

Walkthroughs
A walkthrough was written by Aubrey Kalyn Chen aka SwiftHeart Dragon on 12th June 1995. It can be found:
Here [TXT] at the Ultima Web Archive
Here [HTML conversion by Olray Dragon] at SwiftHeart Dragon's website.
Another Walkthrough, by Lim Soo Ling [TXT], is also on the Ultima Web Archive.
The Other Codex (Wayback Machine) has an excellent walkthrough, as well as answers to some Frequently Asked Questions (Wayback Machine).
The Let's Play Archive has a Let's Play Martian Dreams walkthrough. Let's Play walkthroughs are known for the massive amount of screenshots accompanying them and for the humourous approach taken in completing the games.
Videos
Some Martian Dreams videos on YouTube include:
"Worlds of Ultima - Martian dreams" by piajeno - game introduction and some gameplay.
"ORIGIN Systems - Ultima : WoA - Martian Dreams (Intro 1)" by BloodPigggy - game introduction.
"ORIGIN Systems - Ultima: WoA - Martian Dreams (Gameplay 1)" by BloodPigggy - gameplay and dialogue.


&copy; by Daniel D'Agostino 2002-2021