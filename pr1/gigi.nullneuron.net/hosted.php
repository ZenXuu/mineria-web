Dino's Ultima Page - Hosted Sites












&#9776; Navigation


Main

Latest News
Old News
Credits


Feedback

Contact
Discussion


Links

WebDB2
Hosted Sites


Information

General
Ultima I
Ultima II
Ultima III
Ultima IV
Ultima V
Ultima VI
Savage Empire
Martian Dreams
Underworld I
Ultima VII
Underworld II
Serpent Isle
Ultima VIII
Ultima IX
Mount Drash


0xdeadbeef

Ultima X
Lord of Ultima




Hosted Sites
Hosted websites
The following websites are hosted on the same server as Dino's Ultima Page,
but are still maintained by their respective webmasters.

Ultima Thule! by Tovi Almozlino (Infinitron Dragon)
Ultima Tech Info and Patches by Marc Winterrowd (Nodling Dragon)
Kobra Kai's homepage by Elton Kheid Takara (Kobra Kai Dragon)
Kenneth Whitten's homepage by Kenneth Whitten (Lumina Dragon)

Dead websites
The following are websites I restored to the web to prevent them from
disappearing completely. They are no longer supported by their respective
webmasters, who are nonetheless free to reclaim control over them if they
wish.

UltimaInfo, based on a 2008 backup. Hosted with permission from Paulon Dragon.
The Ultima Web Archive, based on a 2008 backup
The Lost Vale by Stuart Kerrigan (Amazing Dragon)
Dino's Ultima Page (old version as at January 2003) by Daniel D'Agostino (Dino the Dark Dragon) - retrieved from Geocities on 11th October 2009

Former Geocities websites
A large quantity of ancient and useful Ultima websites went down when
Geocities was closed in 2009. Some of these have been salvaged at the following
locations:

Bootstrike's archive
Oocities
I have also saved a bunch of ex-Geocities websites, which I hope to restore to the web one day.


&copy; by Daniel D'Agostino 2002-2021