Dino's Ultima Page - Credits












&#9776; Navigation


Main

Latest News
Old News
Credits


Feedback

Contact
Discussion


Links

WebDB2
Hosted Sites


Information

General
Ultima I
Ultima II
Ultima III
Ultima IV
Ultima V
Ultima VI
Savage Empire
Martian Dreams
Underworld I
Ultima VII
Underworld II
Serpent Isle
Ultima VIII
Ultima IX
Mount Drash


0xdeadbeef

Ultima X
Lord of Ultima




Credits
Contents

Special Thanks
Linkers
Information Submitters
Mistake Detectors
Testers of the Fifth Design

Special Thanks
Special thanks goes to:

Hacki Dragon for guidelines on web design
Shadow of Light Dragon for whom I originally made this website
Edbgon for the shiftedphase webspace, and PHP help
Crowley is a major contributor here; see below for the amount of information he contributed
Petrell is a news poster here at Dino's Ultima Page, and before that, he had been posting news at some topics [1, 2, 3] in my forum while I was busy with University
Withstand the Fury Dragon, who also posted news at Dino's Ultima Page, and with whom I sometimes collaborate on various Ultima-related issues

Linkers
Thanks goes to those who linked to Dino's Ultima Page.
Still Alive
The following sites that have linked to Dino's Ultima Page are still around.

Hacki Dragon, webmaster of Hacki's Ultima Page
Xe Dragon, webmaster of HRUMP
Andrew Taylor, webmaster of xu4
Sergorn Dragon, webmaster of La Légende d' Ultima
Petrell, webmaster of Petrell's Domain
Shadow of Light Dragon, webmistress of Shadow of Light Dragon's Weyr
Kobra Kai, webmaster of Kobra Kai Dragon's Guide to Ultima 8
Infinitron Dragon, webmaster of Ultima Thule!
Johnny Wood, webmaster of Shattered Moon (Ultima IV Multiplayer and Classic Ultima Online)
Galleon Dragon, webmaster of Ultima 6 Online
The webmaster of Project Britannia
Marzo Sette Torres Junior, webmaster of Seven Towers
The community of the Codex of Ultima Wisdom
Hawkwind, webmaster of Forgotten World
Jaesun, webmaster of NWN2 Ultima VII Part 2
Withstand the Fury Dragon, webmaster of Aiera
Chris Hopkins, developer of Ultima IV Part 2 and the Adventure Creation Kit
Sergorn Dragon, leader of Return to the Serpent Isle
Sergorn Dragon, webmaster of Ultima Adventures
Voyager Dragon, webmaster of Ultima: The Reconstruction
The webmaster of Chicken George's Mods
Houston Dragon, webmaster of Houston Dragon's Ultima Page
Sir Cabirus Dragon, webmaster of Sir Cabirus Ultima Homepage

Dead Sites
The following sites that have linked to Dino's Ultima Page have since disappeared.
You can find their original locations on the Ultima Web Database 2,
and use those links in the Wayback Machine to see
what they originally looked like.

Edbgon, webmaster of Ultima VIII - Exile
Wind Walker, webmaster of Adventures of Blackthorn
Myran, webmaster of Enilno
Silva Dragon, webmistress of Silva's World
Withstand the Fury Dragon, webmaster of Lost Sosaria
Commander Falcon, webmaster of Falcon Designs
The webmaster of Ultima VII: The Chronicles of Batlin
Guru Dragon, webmaster of Guru Dragon's Ultima section
Joe and Paula Garrity, webmasters of The Origin Museum
Withstand the Fury Dragon, webmaster of Time Immortal
Corv, webmaster of the Titans of Ether
The webmaster of The Ultimate Game Guide (Wayback Machine link)
The webmaster of Agape
Galadis, webmaster of Fantasy Art Comics
Kevin Fishburne, webmaster of Ultima 5 for Morrowind
Covenant, webmaster of C0venant.NET
Paulon Dragon, maintainer of the Ultima Gameplay FAQ (Wayback Machine link)
Santiago Zapata, webmaster of the Villa of Darkness (Wayback Machine link)
Very Ancient Dragon, webmaster of The Very Ancient Dragon's Ultima I: First Age of Darkness Compendium
Thepal, webmaster of Ultima IX: Infinity Eternal
The webmaster of Ultima 8 Online

Information Submitters
Thanks goes to those who submitted information for Dino's Ultima Page:

Natreg Dragon provided me with a link to the Ultima I Shrine on the RPG Classics Shrines. All the links to pages there are in my Ultima 1 Guide thanks to him.
Hacki Dragon submitted all the music links on my General Information page.
Gerry gave me a link to an old Lost Vale advert with more information about this hidden land.
Andrew Taylor pointed out the huge map and file format information on xu4.
Crowley has posted a whole list of prices of Ultima 6 weapons and armour, pointed out an error with the Lord British page, added a contribution to the copy protection page and wrote the Guardian's Antics as seen in UW2!
Tom Vorwerk has told me about something hidden behind the west wall of the Tenebrae Library in Ultima 8, which turned out to be 2 chairs.
Infinitron Dragon provided five screenshots of a ghost ship in Britain in Ultima 9. He also pointed out the existence of Ultima IV Reborn, an Ultima 4 remake for Neverwinter Nights.
David Randall pointed out a site containing all the Ultima 5 town maps.

I must also thank all those who gave their input in the respective guides, and who
are not mentioned here but in those respective guides.
Mistake Detectors
Thanks goes to those who pointed out mistakes on the site:

Kobra Kai who pointed out some errors in this thread as well as broken links.
Kenn Rice pointed out a broken link in the Martian Dreams info page.
Crowley pointed out a broken link in the Serpent Isle page.

Testers of the Fifth Design
Thanks goes to those who tested the fifth design of Dino's Ultima Page:

Withstand the Fury Dragon
Gul
Kobra Kai
Crowley


&copy; by Daniel D'Agostino 2002-2021