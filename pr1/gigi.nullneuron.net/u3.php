Dino's Ultima Page - Ultima III (Exodus)












&#9776; Navigation


Main

Latest News
Old News
Credits


Feedback

Contact
Discussion


Links

WebDB2
Hosted Sites


Information

General
Ultima I
Ultima II
Ultima III
Ultima IV
Ultima V
Ultima VI
Savage Empire
Martian Dreams
Underworld I
Ultima VII
Underworld II
Serpent Isle
Ultima VIII
Ultima IX
Mount Drash


0xdeadbeef

Ultima X
Lord of Ultima




Ultima III - Exodus
Bugs and Mistakes
Ultima 3 inaccuracies [English | German] can be found at Hacki's Ultima Page [English | German].
Concentrations
Ultima 3 sections can be found:
Here at the Ultima Web Archive
Here at Auric's Ultima Moongates
Here at the Codex of Ultima Wisdom.
LairWare's Ultima 3 at LairWare

Documentation
The Ultima III Encore CD-ROM Game Manual is on the Ultima Web Archive.
An Ultima 3 Manual can be found at LairWare's Ultima 3.
Not exactly documentation but similar, there is a list of other commands used in the game, on the Other Codex (Internet Archive).
Downloads
Obtaining Ultima 3
The first Ultima trilogy (including Ultima 3) can be purchased from GOG.com.
Ultima 3 can also be played online as part of the MS-DOS Games Archive.
Mac Version
The Macintosh port, written by Leon McNeill, can be downloaded:
From LairWare
Here at Auric's Ultima Moongates

Music
There is Ultima III music on the music page in Auric's Ultima Moongates.
All the Ultima III music is on the Bard's Library.
File Format Information
The C64 tileset of Ultima 3 was extracted by
Minstrel Dragon, who kindly agreed to have it hosted here at Dino's Ultima Page.
Magic
LairWare's Ultima 3 has the following pages containing documentation of spells:
The Ancient Liturgy of Truth (Cleric spells)
The Book of Amber Runes (Wizard spells)

Music
Mysterious Sosaria's Bard's Library has a section on Ultima 3 music.
Items
A list of prices is on the Other Codex (Internet Archive).
Maps and Places
The cloth world map can be found:
At the High Resolution Ultima Map Project
Here at the Ultima Web Archive
Here at Auric's Ultima Moongates
Here (two maps) at Codex of Ultima Wisdom
Here at LairWare's Ultima 3
There are excellent Ultima III maps of the world and of the city of Grey on the Ultimatrix Homepage, taken with Ultimatrix.
UltimaInfo has a page called Ultima 3: The Lay of the Land (Internet Archive), which has annotated digital maps of just about everything (including Sosaria, Ambrosia, the two castles and the ten towns).
Enlightenment Dragon created a copy of the Ultima 3 world map using the Ultima 5 tileset in Excel. You can find this map below, and it is also mirrored at The Ultima Codex.



Pictures
Auric's Ultima Moongates hosts an old advertisement scanned by Auric from an old gaming magazine.
Helen Garriott's original and unused concept art for the Ultima 3 box was released at The Ultima Codex. See the news entry about it, or read more and download the art from its Origin Gallery entry.
The Ultima Codex released a picture of some early handwritten Assembly language source code for Ultima 3. Check out the news about it or the Origin Gallery entry.
Projects
LairWare's Ultima 3 is a "modern Macintosh update" of Ultima 3. It is shareware in the sense that it's free but it costs $5 to unlock Ambrosia, which is required to finish the game.
Ultimore: A World Divided is an unofficial expansion to Ultima 3. Read more about it at:
"Were there additional scenarios/maps made for Ultima 3?" Q&amp;A at The Ultima Codex
"Ultimore: A World Divided – Part 1" at Pix's Origin Adventures
"Pix Plays Ultimore: A World Divided" news at The Ultima Codex (with additional info in the comments)

Reviews
The MS-DOS Project has a 20-minute video review of Ultima 3.
Summaries
The story can be found:
At the Ultima 3 page of the Codex of Ultima Wisdom
Here at Auric's Ultima Moongates

Technical
Among the Technical Info at the Ultimatrix Homepage, one can find the following file formats:
Dungeon files
Tiles files
Towns files
Map file
Gargish Dragon's Ultima Page has the Ultima 3 Roster file format, courtesy of Sam Glasby.
Marc Winterrowd hosts, among his Ultima Tech Info and Patches, an incomplete but nonetheless detailed description of the Ultima 3 file formats [TXT].
Trivia
Some Ultima 3 trivia can be found at the Ultima 3 page at the Codex of Ultima Wisdom.
Walkthroughs
The Ultima III Help and Information File by Warlord, on the Ultima Web Archive, gives lots of useful information and what is needed to finish Ultima III.
It is also found in TXT format on Auric's Ultima Moongates.
There is a better hint file by Leon McNeill, illustrated with pictures to accompany the information.
The Other Codex tells you about the cards needed to destroy Exodus (Internet Archive).


&copy; by Daniel D'Agostino 2002-2021