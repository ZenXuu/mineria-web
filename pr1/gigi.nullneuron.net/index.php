Dino's Ultima Page - News















&#9776; Navigation


Main

Latest News
Old News
Credits


Feedback

Contact
Discussion


Links

WebDB2
Hosted Sites


Information

General
Ultima I
Ultima II
Ultima III
Ultima IV
Ultima V
Ultima VI
Savage Empire
Martian Dreams
Underworld I
Ultima VII
Underworld II
Serpent Isle
Ultima VIII
Ultima IX
Mount Drash


0xdeadbeef

Ultima X
Lord of Ultima




Main
News articles
The Bread Gate
24th February 2021 by Dino
About a week ago, Yulo Tomorrow released The Bread Gate, a parody of the bread-baking job in Ultima 7: The Black Gate. In this game, made with Unity 3D, you have to frantically bake loaves of bread to both prevent Iolo from starving and make enough extra money to buy a candelabra. You can play the game online or download it.
It's also worth mentioning that Yulo Tomorrow's Itch page has art that's very reminiscent of the intro scene from Ultima 6, and she's also active on Twitter, where she shares both Ultima-related fan art, and her progress playing through Ultima 7: The Black Gate.
The Digital Antiquarian on Ultima 8
21st February 2021 by Dino
A couple of days ago, The Digital Antiquarian published "Ultima VIII (or, How to Destroy a Gaming Franchise in One Easy Step)", his lengthy treatise on the historical context in which Ultima 8 was developed, and its resulting disastrous critical reception.
UltimaInfo restored
13th February 2021 by Dino
Following the restoration of the Ultima Web Archive a week ago, I am delighted to have also returned UltimaInfo to the web today.
Probably one of the best and most comprehensive Ultima information sites on the web, and best known for its encyclopaedic The Other Codex, UltimaInfo was created and run by Paulon Dragon until it disappeared from the web a few years ago.
It is now being hosted alongside Dino's Ultima Page with Paulon Dragon's blessing. Its WebDB2 entry has been updated accordingly, and you can also get to it via the Hosted Sites page. In time, I will update all links to UltimaInfo on Dino's Ultima Page to point to the new address.
Older News
See the News page for older news and articles.


&copy; by Daniel D'Agostino 2002-2021