Dino's Ultima Page - Ultima Underworld 1: The Stygian Abyss












&#9776; Navigation


Main

Latest News
Old News
Credits


Feedback

Contact
Discussion


Links

WebDB2
Hosted Sites


Information

General
Ultima I
Ultima II
Ultima III
Ultima IV
Ultima V
Ultima VI
Savage Empire
Martian Dreams
Underworld I
Ultima VII
Underworld II
Serpent Isle
Ultima VIII
Ultima IX
Mount Drash


0xdeadbeef

Ultima X
Lord of Ultima




Ultima Underworld 1 - The Stygian Abyss
Bugs and Mistakes
Ultima Underworld I inaccuracies [English | German] can be found at Hacki's Ultima Page [English | German].
See the Fun section for some known bugs.
Concentrations
You can find Ultima Underworld I sections of websites:
Here at Sir Cabirus' Ultima Homepage which includes a detailed walkthrough, a download of the Sir Cabirus patch that enables Ultima Underworld to run on modern systems, maps, pictures and other goodies
Here at the Ultima Web Archive with a walkthrough, music and downloads of a patch and a demo of the first level of the Abyss
Here at the Other Codex (Internet Archive) with a list of the undocumented spells and links to other resources
At Arthuris Dragon's Ultima Maps and Tables

Culture
My Lizardman Language page contains a dictionary
showing Lizardman to English words, and a translator tool useful for quickly translating
Lizardman text.
Development
"Life Off
the Grid, Part 1: Making Ultima Underworld" is a detailed account of the dynamics involved in the
development of Ultima Underworld, including the background of those involved, technological breakthroughs,
and a rough but ultimately fruitful relationship with Origin.
Ultima Underworld pioneered the technique of texture mapping. In his book "Masters of Doom", David Kushner
talks about how texture mapping made its way into id Software's games thanks to Ultima Underworld:

"Texture mapping meant applying a detailed pattern or texture to a tile of graphics on the computer screen. Instead of drawing a solid color on the back wall of a game, the computer would draw a pattern of bricks. Romero heard about texture mapping from Paul Neurath, whose company, Blue Sky Productions, was working on a game called Ultima Underworld, which would be published by Richard Garriott's Origin company. Neurath told Romero that they were applying texture mapping to shapes or polygons in a three-dimensional world. Cool, Romero thought. When he hung up the phone, he spun his chair to Carmack and said, 'Paul said he's doing a game using texture mapping.'
"'Texture mapping?' Carmack replied, then took a few seconds to spin the concept around in his head. 'I can do that.'"
-- Masters of Doom, by David Kushner (2003), page 89

The book goes on to explain how this resulted in the game Catacomb 3D, which was released six months before
Ultima Underworld.
Documentation
The Ultima Underworld I manual, "Memoires of Cabirus", can be found:

[TXT] at Quill Dragon's Lair.
Here [TXT in HTML] at Bootstrike (taken from Quill Dragon's Lair).

A picture of an early
version of "Memoirs of Cabirus" can be found at Pix's
Origin Adventures.
Dan Simpson's list of keyboard commands can be found as follows:
Version 1.31 (22nd January 2005) at Bootstrike's Ultima Worlds
Version 1.3 [TXT] (26th March 2000) at Underworld Adventures

File Formats
Ultima Underworld file format information was compiled as part of the Underworld Adventures project.
This documentation can be found as follows:

Version 1.72 (7th May 2019) at the new Underworld Adventures homepage on GitHub
Version 1.68 (11th April 2006) at Bootstrike's Ultima Worlds

Fun
Warren



In the southwest corner of Level 6, there is "an upset spectre named Warren",
clearly an easter egg for Warren Spector, the producer of Ultima Underworld.

"Our producer from Origin was Warren Spector, already a pretty important guy in the industry but someone who has since received yet more accolades for working on, well, Ultima Underworld, not to mention other games such as System Shock (with us), Deus Ex, and Epic Mickey. He is a super down-to-earth guy and we wasted no time in making fun of him, which he took with impressive grace. I guess it was a tradition at Origin to insert characters based on Warren into their games, so we figured we had to as well. Luckily we already had a “spectre” type of monster so it took no work to name one of them Warren. We made sure that Warren was in town the day that Tim was reading through the daily bug report list and said "Bug: There is no reference in the game to Warren Spector", to which the rest of us immediately piped up, "Fixed!" without further explanation, much to Warren’s chagrin."
-- Dan Schmidt, "Ultima Underworld bugs" (21st February 2011)

Other Bugs and In-Game Jokes
Dan Schmidt's blog article "Ultima
Underworld bugs" (21st February 2011) talks about various bugs as well as intentional in-game jokes
and references, including:

Warren the spectre
Psychedelic effects when eating mushrooms
A joke involving the Apple II palette
An in-game Pacman reference
Judy falling into the lava
The red sports car

There's another story on his blog about the ghoul Eyesnack:

"One of the levels (5, I think?) was largely populated by ghouls, with standard flesh-eating names like Eyesnack and Kneenibble. Naturally you could talk to them instead of just fighting them. Jon Maiara (the same guy responsible for the Pac-Man homage) was writing the conversations for them, and included all sorts of things like the opportunity for you to make fun of Eyesnack’s name, to which he would respond by making fun of your name in return. You see the edge case, of course, right?
That’s right, part of our precious 640K was devoted to checking for whether the player’s name is also Eyesnack, in which case, in response to your mockery, the ghoul proclaims indignantly, "But your name same as mine!""
-- Dan Schmidt, "One more Ultima Underworld story" (17th March 2011)

F10 Cheats
Some sites report that you can use four
key combinations starting with F10
in order to enable god mode, add more light, skip levels, and stop time.
These don't seem to work (mainly because F10 is the shortcut that makes
you rest), and are most likely fake.
Unlimited Light
Delete or rename the SHADES.DAT file, and you will never be in darkness again.
Unlimited Scrolls and Potions
Use a fireball spell on a scroll or potion. This reduces them to debris, but they
do not use their power. You can then use them as much as you like without them ever
getting consumed.
Gameplay
The Digital Antiquarian has written
"Life
Off the Grid, Part 2: Playing Ultima Underworld", a detailed account of Ultima Underworld's
gameplay and why it was so groundbreaking for its time.
The game's gameplay is demonstrated by a Japanese
commercial [YouTube video] which was recovered and uploaded by Dominus Dragon.
Items
Screenshots of the incense-induced visions of the Cup of Wonder: [1 | 2 | 3]
Crowley provided annotated maps [ZIP] with locations of all magical equipment in Ultima Underworld 1, with the following information:

Crowley writes: Armor can be enchanted for increased toughness or protection, and weapons
for damage or accuracy. These have the following levels:
Minor
-
Additional
Major
Great
Very Great
Tremendous
Unsurpassed
I used some abbreviations on the maps but those should be easily
understandable. I have no idea about any numerical values associated, so I
can't really say if for example a Dagger of Very Great Damage does more
damage than a non-magical longsword. In addition there are a few magical
items that cause the spell effect their name implies when worn, and those
are rather self-explanatory. Perhaps the most obscure one is the Crown of
Maze Navigation on level 7. There is a maze on that same level where
stepping in the wrong place causes instant death. Wearing the crown shows
the safe path. Also there is cursed armor and rings which cause damage when
worn, but I didn't list those.

Combining Items

Drag corn onto a torch to make popcorn.
Use an oil flask on a piece of wood to make a torch.
Combine a pole and strong thread to make a fishing pole.

Lists of Runestone Locations

Here [TXT] at Arthuris Dragon's Ultima Maps and Tables.
Here at Sir Cabirus' Ultima Homepage

More from Sir Cabirus
Other item information pages at Sir Cabirus' Ultima Homepage include:

The Tripartite Key.
The Talismans of Sir Cabirus.
Keys (with locations).
Magical Rings (with locations).

Unique Items
An incomplete list of some unique items in the game:
large gold nugget
silver seed
moonstone
resilient sphere (2x)
crystal splinter

Magic
Magic Reference
My Ultima Underworld 1 Magic page is a reference on
spells and runes used within the Stygian Abyss. It actually allows you to pick the
runes available to you from the rune bag, and highlights the spells you can cast with
those runes.
Undocumented Spells
Eight spells are not listed in the manual. You can find a list of these spells:
At the Other Codex's Ultima Underworld section (Internet Archive)
Here at Sir Cabirus' Ultima Homepage

All Spells
Lists of all the spells can be found:

Here (by Gamma Dragon) at Bootstrike.
Here [TXT] at Arthuris Dragon's Ultima Maps and Tables.

Maps
Maps of every level can be found:
At Arthuris Dragon's Ultima Maps and Tables
At the Ultima Web Archive's Maps of Ultima Underworld 1
At each page of Sir Cabirus' walkthrough
Here (as seen with the Underworld Viewer) at Sir Cabirus' Ultima Homepage
Here as part of the planning for Gamma Dragon's speedrun
At Goldenflame Dragon's Ultima Underworld walkthrough (some maps are incomplete)
In Crowley's annotated maps [ZIP], hosted here at Dino's Ultima Page, showing the locations of all magical equipment. Level 1 is the only one missing.
Maps of the first level only can be found:
Here of the Abyss is on the Ultima Web Archive
Here made up of U4-style tiles, as part of LRUM Project

Music
A zip file containing Ultima Underworld music is available for download from the Ultima Web Archive's Ultima Music page.
Individual tracks from the Ultima Underworld music can be downloaded from The Bard's Library, a section of Mysterious Sosaria.
Other Versions
There was a version of Ultima Underworld for PocketPC (Wayback Machine link), and there's a video on YouTube showing gameplay (links contributed by Vividos).
People
A page about Tyball and Garamon, taken from the Ultima Underworld cluebook "Mysteries of the Abyss", can be found at Sir Cabirus' Ultima Homepage.
Related Projects

Underworld Adventures - remake that was discontinued in 2007. It was originally hosted at SourceForge but moved to a new home on GitHub in February 2019.
Ultima Underworld Viewer - level viewer by Peroxide, who were behind the development of the now discontinued Ultima 1: A Legend is Reborn project
Underworld Game Creation Kit - also by Peroxide, but apparently no links work any more
If you need to run Ultima Underworld 1 or 2 on modern systems, you can download Sir Cabirus' Ultima Underworld patch from the Ultima Underworld section at Sir Cabirus' Ultima Homepage.
...more projects are listed at The Ultima Codex
Underworld Ascendant is a crowdfunded project by OtherSide Entertainment to create a spiritual successor for Ultima Underworld.

Reviews
Rampant Coyote wrote a "Reminiscing about Ultima Underworld" piece on 3rd June 2011.
A review by Russ Ceccola appeared in the June-July 1992 edition of Questbusters [PDF].
In a 2014 video by IGN, Tal Blevins talks about why Ultima Underworld is his all-time favourite game.
This review by Rosemary Young (September 2007) includes three screenshots from the game.
Walkthroughs
The Ultima Underworld Walkthrough on Avocet Dragon's Journey to Sosaria is an excellent guide because apart from explaining what is to be done, the walkthrough shows maps of each area. Although the site is long gone, a copy of the walkthrough can be found at Bootstrike.
There is another walkthrough for Ultima Underworld at Sir Cabirus' Ultima Homepage in great detail.
A walkthrough for Ultima Underworld by Mitch Aigner is available in:
English at the Ultima Web Archive
English at Bootstrike
English at TTLG's Ultima Underworld Series Central Chamber (via the Internet Archive)
French at La Legende d' Ultima (via the Internet Archive)
English [TXT] at Underworld Adventures
According to Bootstrike, these Ultima Underworld I Tips by Lord DCD make up the first Ultima Underworld I walkthrough, dating back to 1992.
Goldenflame Dragon's Ultima Underworld walkthrough was written during a playthrough and covers what is necessary to finish the game. It includes incomplete maps of each level as well as a list of mantras.
Kikoskia's Let's Play Ultima Underworld is a video playthrough of the game (spotted by Vividos).
Speedruns

WhoVideo LinkLengthYear
NinYouTube video / explanation [TXT] / video and explanation16:392015
Gamma DragonYouTube video / YouTube video / planning17:252012
lagdotcomN/A (see Speedrun entry)21:102014
thecybercat/GunnarPart 1 | Part 2 | Part 3 (YouTube) / Flash (Bootstrike)~30mins2008

Lists of Mantras
Lists of mantras can be found:

Here [TXT] at Arthuris Dragon's Ultima Maps and Tables.
In the Ultima Underworld I Tips by Lord DCD, hosted by Bootstrike.
At the end of Goldenflame Dragon's Ultima Underworld walkthrough


&copy; by Daniel D'Agostino 2002-2021