Dino's Ultima Page - Contact












&#9776; Navigation


Main

Latest News
Old News
Credits


Feedback

Contact
Discussion


Links

WebDB2
Hosted Sites


Information

General
Ultima I
Ultima II
Ultima III
Ultima IV
Ultima V
Ultima VI
Savage Empire
Martian Dreams
Underworld I
Ultima VII
Underworld II
Serpent Isle
Ultima VIII
Ultima IX
Mount Drash


0xdeadbeef

Ultima X
Lord of Ultima




Contact
Before you start writing...






Do not ask me to send you or otherwise provide any of the Ultima games. I will not.
In case you have any difficulty communicating in English, I also speak Maltese, Italian and French.
If you are playing Ultima 9 and have lost the Rune of Compassion after cleansing the Shrine of Compassion, it is normally floating in the air above you.
If you're trying to run Ultima 8 under Windows (or another modern operating system), try Pentagram (in progress) or DOSBox (complete)
If you want to tell me about some Ultima news, please do!
If you want to help out with posting Ultima news on the site, contact me.

Tell me
My email is dandago [at] gmail [dot] com.


&copy; by Daniel D'Agostino 2002-2021